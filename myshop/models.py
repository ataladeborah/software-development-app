from django.db import models
import datetime 
# categories of Products
class Category(models.Model):
    name = models.CharField(max_length = 50)

    def __str__(self):
        return self.name
    

#all of our products
class Product(models.Model):  
    name = models.CharField(max_length = 100)
    price = models.DecimalField(default=0, decimal_places=2, max_digits=15 )
    category = models.ForeignKey(Category, on_delete=models.CASCADE, default=1)
    description = models.CharField(max_length = 250, default='', blank= True, null= True)
    image = models.CharField(max_length=2083)
    is_sale = models.BooleanField(default=False)
    sale_price = models.DecimalField(default=0, decimal_places=2, max_digits=15 ) 


    def __str__(self):
        return self.name



